# Lofi-Player
A lofi audio player made using Qt

It uses the online radio by https://laut.fm/lofi.

[link to the radio](https://laut.fm/lofi)

all credits goes to them.

# ScreenShot
![Screenshot](https://gitlab.com/Roshan-R/Lofi-Player/-/raw/336460c837df9e2bde3a8de6026da5430062ed1e/imgs/Screenshot.jpeg)
